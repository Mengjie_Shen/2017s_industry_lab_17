package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mshe666 on 15/01/2018.
 */
public class SimpleSpellCheckerTest {

    private SimpleSpellChecker simpleSpellChecker;
    private IDictionary dictionary = new Dictionary();
    private String wordsToCheck = " ";

    @Before
    public void setUp() {
        try {
            simpleSpellChecker = new SimpleSpellChecker(dictionary, wordsToCheck);
        }catch (InvalidDataFormatException e) {
            fail();
        }
    }

    @Test
    public void testSimpleSpellCheckerConstructor() {

        String[] words = wordsToCheck.split("[\\s\\W]+");

        for (String word : words) {
            assertFalse(dictionary.isSpellingCorrect(word));
        }
    }

    @Test
    public void testGetMisspelledWordsWithWordsFromDic() {
        wordsToCheck = "the be";

        try {
            simpleSpellChecker = new SimpleSpellChecker(dictionary, wordsToCheck);
        }catch (InvalidDataFormatException e) {
            fail();
        }

        List<String> misspelled = new ArrayList<>();

        assertEquals(misspelled, simpleSpellChecker.getMisspelledWords());

    }

    @Test
    public void testGetMisspelledWordsWithWordsNotFromDic() {
        wordsToCheck = "ict auckland";

        try {
            simpleSpellChecker = new SimpleSpellChecker(dictionary, wordsToCheck);
        }catch (InvalidDataFormatException e) {
            fail();
        }

        List<String> misspelled = new ArrayList<>();
        String[] words = wordsToCheck.split("[\\s\\W]+");
        for (int i = 0; i < words.length; i++) {
            misspelled.add(words[i]);
        }

        Collections.sort(misspelled);
        Collections.sort(simpleSpellChecker.getMisspelledWords());

        assertEquals(misspelled, simpleSpellChecker.getMisspelledWords());

    }

    @Test
    public void testGetUniqueWords() {
        List<String> uniqueWords = new ArrayList<>();
        String[] words = wordsToCheck.split("[\\s\\W]+");
        for (int i = 0; i < words.length; i++) {
            if (!uniqueWords.contains(words[i])) {
                uniqueWords.add(words[i]);
            }
        }

        assertEquals(uniqueWords, simpleSpellChecker.getUniqueWords());

    }

    @Test
    public void testGetFrequencyOfWord() {
        String word = "ictgradschool";
        int frequency = 0;
        String[] words = wordsToCheck.split("[\\s\\W]+");
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(word)) {
                frequency++;
            }
        }

        int result = 0;
        try {
            result = simpleSpellChecker.getFrequencyOfWord(word);
        } catch (InvalidDataFormatException e) {
            fail();
        }

        assertEquals(frequency, result);

        word = "exit";
        frequency = 0;

        result = 0;
        try {
            result = simpleSpellChecker.getFrequencyOfWord(word);
            assertEquals(0, result);
        } catch (InvalidDataFormatException e) {
            assertEquals(frequency, result);
        }


    }

}