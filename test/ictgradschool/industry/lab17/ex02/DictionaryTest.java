package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static ictgradschool.industry.lab17.ex02.IDictionary.WORDS;
import static org.junit.Assert.*;

/**
 * Created by mshe666 on 15/01/2018.
 */
public class DictionaryTest {

    private Dictionary dictionary;

    @Before
    public void setUp() {
        dictionary = new Dictionary();
    }

    @Test
    public void testDictionaryConstructor() {
        //test words in dictionary
        String word = "the";
        assertTrue(dictionary.isSpellingCorrect(word));

        word = "dictate";
        assertTrue(dictionary.isSpellingCorrect(word));

        word = "perceived";
        assertTrue(dictionary.isSpellingCorrect(word));

        word = "bonus";
        assertTrue(dictionary.isSpellingCorrect(word));

        //test words not in dictionary
        word = "contrary";
        assertFalse(dictionary.isSpellingCorrect(word));

        word = "typesetting";
        assertFalse(dictionary.isSpellingCorrect(word));

        word = "injected";
        assertFalse(dictionary.isSpellingCorrect(word));

        word = "treatise";
        assertFalse(dictionary.isSpellingCorrect(word));

    }

    @Test
    public void testIsSpellingCorrect() {
        //test words in dictionary
        String word = "the";
        assertTrue(dictionary.isSpellingCorrect(word));

        word = "bonus";
        assertTrue(dictionary.isSpellingCorrect(word));

        //test words not in dictionary
        word = "contrary";
        assertFalse(dictionary.isSpellingCorrect(word));

        word = "treatise";
        assertFalse(dictionary.isSpellingCorrect(word));
    }



}