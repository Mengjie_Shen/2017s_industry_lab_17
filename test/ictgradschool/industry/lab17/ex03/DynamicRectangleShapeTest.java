package ictgradschool.industry.lab17.ex03;

import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

/**
 * Created by mshe666 on 15/01/2018.
 */
public class DynamicRectangleShapeTest {

    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }

    @Test
    public void testDefaultConstructor() {
        DynamicRectangleShape shape = new DynamicRectangleShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        // Check that the paint method caused a rectangle at position (0, 0), with size (25, 35), to be drawn.
        shape.paint(painter);
        assertEquals("(rectangle 0,0,25,35)", painter.toString());

        assertEquals(shape.getColor(), painter.getColor());
    }

    @Test
    public void testConstructorWithSpeedValues() {
        DynamicRectangleShape shape = new DynamicRectangleShape(1, 2, 3, 4);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(rectangle 1,2,25,35)", painter.toString());

        assertEquals(shape.getColor(), painter.getColor());
    }

    @Test
    public void testConstructorWithSpeedAndWHValues() {
        DynamicRectangleShape shape = new DynamicRectangleShape(1, 2, 3, 4, 5, 6);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());

        shape.paint(painter);
        assertEquals("(rectangle 1,2,5,6)", painter.toString());

        assertEquals(shape.getColor(), painter.getColor());
    }

    @Test
    public void testConstructorWithAllValues() {
        DynamicRectangleShape shape = new DynamicRectangleShape(1, 2, 3, 4, 5, 6, Color.black);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());
        assertEquals(Color.black, shape.getColor());

        shape.paint(painter);
        assertEquals("(rectangle 1,2,5,6)", painter.toString());

        assertEquals(shape.getColor(), painter.getColor());
    }

    @Test
    public void testShapeMoveWithBounceOffRight() {
        DynamicRectangleShape shape = new DynamicRectangleShape(100, 20, 12, 15);
        if (shape.getColor().equals(Color.))
        shape.paint(painter);
        shape.move(135, 10000);
        shape.setColor(Color.red);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);

        assertEquals("(rectangle 100,20,25,35)" +
                "(getColor 0,0,0)(setColor 255,0,0)(fillRect 110,35,25,35)(setColor 0,0,0)(rectangle 110,35,25,35)" +
                "(getColor 0,0,0)(setColor 255,0,0)(fillRect 98,50,25,35)(setColor 0,0,0)(rectangle 98,50,25,35)", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffLeft() {
        DynamicRectangleShape shape = new DynamicRectangleShape(10, 20, -12, 15);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.setColor(Color.red);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(rectangle 10,20,25,35)" +
                "(getColor 0,0,0)(setColor 255,0,0)(fillRect 0,35,25,35)(setColor 0,0,0)(rectangle 0,35,25,35)" +
                "(getColor 0,0,0)(setColor 255,0,0)(fillRect 12,50,25,35)(setColor 0,0,0)(rectangle 12,50,25,35)", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffTop() {
        DynamicRectangleShape shape = new DynamicRectangleShape(10, 10, 0, -15);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.setColor(Color.white);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(rectangle 10,10,25,35)(rectangle 10,0,25,35)(rectangle 10,15,25,35)", painter.toString());
    }

}