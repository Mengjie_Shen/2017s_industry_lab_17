package ictgradschool.industry.lab17.ex03;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by mshe666 on 15/01/2018.
 */
public class OvalShapeTest {

    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }

    @Test
    public void testDefaultConstructor() {
        OvalShape shape = new OvalShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        // Check that the paint method caused a rectangle at position (0, 0), with size (25, 35), to be drawn.
        shape.paint(painter);
        assertEquals("(oval 0,0,25,35)", painter.toString());
    }

    @Test
    public void testConstructorWithSpeedValues() {
        OvalShape shape = new OvalShape(1, 2, 3, 4);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(oval 1,2,25,35)", painter.toString());
    }

    @Test
    public void testConstructorWithAllValues() {
        OvalShape shape = new OvalShape(1, 2, 3, 4, 5, 6);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());

        shape.paint(painter);
        assertEquals("(oval 1,2,5,6)", painter.toString());
    }

}