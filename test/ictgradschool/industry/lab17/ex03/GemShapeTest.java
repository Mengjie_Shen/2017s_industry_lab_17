package ictgradschool.industry.lab17.ex03;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by mshe666 on 15/01/2018.
 */
public class GemShapeTest {
    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }

    @Test
    public void testDefaultConstructor() {
        GemShape shape = new GemShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        // Check that the paint method caused a rectangle at position (0, 0), with size (25, 35), to be drawn.
        shape.paint(painter);
        //"(polygon xpoints: " + Arrays.toString(polygon.xpoints) + ", ypoints: " + Arrays.toString(polygon.ypoints) + ")"
        assertEquals("(polygon xpoints: [0, 12, 25, 12], ypoints: [17, 0, 17, 35])", painter.toString());
    }

    @Test
    public void testConstructorWithSpeedValues() {
        GemShape shape = new GemShape(1, 2, 3, 4);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 13, 26, 13], ypoints: [19, 2, 19, 37])", painter.toString());
    }

    @Test
    public void testConstructorWithAllValuesWhoseWidthIsLessThan40() {
        GemShape shape = new GemShape(1, 2, 3, 4, 5, 6);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 3, 6, 3], ypoints: [5, 2, 5, 8])", painter.toString());
    }

    @Test
    public void testConstructorWithAllValuesWhoseWidthIsMoreThan40() {
        GemShape shape = new GemShape(1, 2, 3, 4, 50, 60);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(50, shape.getWidth());
        assertEquals(60, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 21, 31, 51, 31, 21, 0, 0], ypoints: [32, 2, 2, 32, 62, 62, 0, 0])", painter.toString());
    }
}