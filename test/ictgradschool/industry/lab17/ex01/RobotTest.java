package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        myRobot.turn();
        System.out.println(myRobot.currentState().direction + "," + myRobot.currentState().row + "," + myRobot.currentState().column);

        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
        }catch (IllegalMoveException e) {
            fail();
        }

        assertEquals(10, myRobot.currentState().column);

        try {
            myRobot.move();
            fail();
        }catch (IllegalMoveException e) {
            assertEquals(10, myRobot.currentState().column);
        }

    }

    @Test
    public void testIllegalMoveSouth() {
        //move to row 1, column 1
        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
        }catch (IllegalMoveException e) {
            fail();
        }

        assertEquals(1, myRobot.currentState().row);

        try {
            myRobot.move();
            fail();
        }catch (IllegalMoveException e) {
            assertEquals(1, myRobot.currentState().row);
        }

        //turn south
        myRobot.turn();
        myRobot.turn();

        System.out.println(myRobot.currentState().direction + "," + myRobot.currentState().row + "," + myRobot.currentState().column);


        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
        }catch (IllegalMoveException e) {
            fail();
        }

        assertEquals(10, myRobot.currentState().row);

        try {
            myRobot.move();
            fail();
        }catch (IllegalMoveException e) {
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveWest() {
        //turn east
        myRobot.turn();
        //move to row 1, column 10
        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
        }catch (IllegalMoveException e) {
            fail();
        }

        assertEquals(10, myRobot.currentState().column);

        try {
            myRobot.move();
            fail();
        }catch (IllegalMoveException e) {
            assertEquals(10, myRobot.currentState().column);
        }

        //turn west
        myRobot.turn();
        myRobot.turn();
        System.out.println(myRobot.currentState().direction + "," + myRobot.currentState().row + "," + myRobot.currentState().column);

        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
        }catch (IllegalMoveException e) {
            fail();
        }

        assertEquals(1, myRobot.currentState().column);

        try {
            myRobot.move();
            fail();
        }catch (IllegalMoveException e) {
            assertEquals(1, myRobot.currentState().column);
        }
    }



    @Test
    public void testTurn() {

        Robot.RobotState robotState = myRobot.states.get(myRobot.states.size() - 1);

        if (robotState.direction.equals(Robot.Direction.East)) {
            myRobot.turn();
            assertEquals(Robot.Direction.South, myRobot.getDirection());

        }
        if (robotState.direction.equals(Robot.Direction.South)) {
            myRobot.turn();
            assertEquals(Robot.Direction.West, myRobot.getDirection());

        }
        if (robotState.direction.equals(Robot.Direction.West)) {
            myRobot.turn();
            assertEquals(Robot.Direction.North, myRobot.getDirection());

        }
        if (robotState.direction.equals(Robot.Direction.North)) {
            myRobot.turn();
            assertEquals(Robot.Direction.East, myRobot.getDirection());
        }
    }

    @Test
    public void testBackTrack() {


        try {
            myRobot.move();
        }catch (IllegalMoveException e) {
            fail();
        }

        myRobot.backTrack();
        assertEquals(myRobot.currentState().direction, Robot.Direction.North);
        assertEquals(myRobot.row(), 10);
        assertEquals(myRobot.column(), 1);


        myRobot.turn();
        myRobot.backTrack();
        assertEquals(myRobot.currentState().direction, Robot.Direction.North);
        assertEquals(myRobot.row(), 10);
        assertEquals(myRobot.column(), 1);


    }
}

